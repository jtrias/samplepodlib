# SamplePodLib

[![Version](http://cocoapod-badges.herokuapp.com/v/SamplePodLib/badge.png)](http://cocoadocs.org/docsets/SamplePodLib)
[![Platform](http://cocoapod-badges.herokuapp.com/p/SamplePodLib/badge.png)](http://cocoadocs.org/docsets/SamplePodLib)

## Usage

To run the example project; clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SamplePodLib is available through [CocoaPods](http://cocoapods.org), to install
it simply add the following line to your Podfile:

    pod "SamplePodLib"

## Author

Juan Trías, juan.trias@intelygenz.com

## License

SamplePodLib is available under the MIT license. See the LICENSE file for more info.

