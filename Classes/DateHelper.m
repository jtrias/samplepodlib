//
//  DateHelper.m
//  vf15
//
//  Created by jtrias on 07/09/13.
//  Copyright (c) 2013 Intelygenz. All rights reserved.
//

#import "DateHelper.h"

@interface DateHelper ()

@property (strong, nonatomic) NSCalendar *calendar;

@end

@implementation DateHelper

- (id)init
{
    self = [super init];
    if (self) {
        self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    }
    
    return self;
}

- (id)initWithTimeZone:(NSString *)timeZone
{
    self = [super init];
    if (self) {
        self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [_calendar setTimeZone:[NSTimeZone timeZoneWithName:timeZone]];
    }
    
    return self;
}

- (NSDate *)dateForMonth:(NSUInteger)month andYear:(NSUInteger)year
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 1;
    components.month = month;
    components.year = year;
    return [_calendar dateFromComponents:components];
}

- (NSDate *)dateByAddingMonths:(NSInteger)months {
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    offsetComponents.month = months;
    return [_calendar dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
}

- (int)currentYear {
    NSDateComponents *components = [_calendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    return components.year;
}

- (int)currentMonth {
    NSDateComponents *components = [_calendar components:NSMonthCalendarUnit fromDate:[NSDate date]];
    return components.month;
}

- (double)millisecondsSince1970ForDate:(NSDate *)date {
    return floor([date timeIntervalSince1970] * 1000);
}

- (NSDateComponents *)dateComponentsForMillisecondsSince1970:(double)millisecondsSince1970 {
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:millisecondsSince1970 / 1000.0f];
    return [_calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:date];
}

@end
