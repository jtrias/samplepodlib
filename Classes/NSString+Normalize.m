//
//  NSString+Normalize.m
//  vf15
//
//  Created by jtrias on 20/09/13.
//  Copyright (c) 2013 Intelygenz. All rights reserved.
//

#import "NSString+Normalize.h"

@implementation NSString (Normalize)

- (NSString *)normalizedString
{
#warning TODO look for a method that really removes the non ASCII chars
    return [[[NSString alloc] initWithData:[self dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES] encoding:NSASCIIStringEncoding] lowercaseString];
    //return [[self stringByReplacingOccurrencesOfString:@"ö" withString:@"o"] lowercaseString];
}

@end
