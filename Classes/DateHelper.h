//
//  DateHelper.h
//  vf15
//
//  Created by jtrias on 07/09/13.
//  Copyright (c) 2013 Intelygenz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject

- (id)initWithTimeZone:(NSString *)timeZone;

- (NSDate *)dateForMonth:(NSUInteger)month andYear:(NSUInteger)year;
- (NSDate *)dateByAddingMonths:(NSInteger)months;
- (int)currentYear;
- (int)currentMonth;
- (double)millisecondsSince1970ForDate:(NSDate *)date;
- (NSDateComponents *)dateComponentsForMillisecondsSince1970:(double)millisecondsSince1970;

@end
