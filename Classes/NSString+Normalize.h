//
//  NSString+Normalize.h
//  vf15
//
//  Created by jtrias on 20/09/13.
//  Copyright (c) 2013 Intelygenz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Normalize)

- (NSString *)normalizedString;

@end
