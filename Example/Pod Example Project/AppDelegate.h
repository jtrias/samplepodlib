//
//  AppDelegate.h
//  Pod Example Project
//
//  Created by Juan on 14/03/14.
//  Copyright (c) 2014 IGZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
